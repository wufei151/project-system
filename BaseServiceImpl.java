package com.soha.traffic.base;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.soha.traffic.bean.PageBean;
import com.soha.traffic.cfg.Configuration;
import com.soha.traffic.util.HqlHelper;

@Transactional
@SuppressWarnings("unchecked")
public abstract class BaseServiceImpl<T> implements BaseService<T> {

	@Resource
	private SessionFactory sessionFactory;
	protected Class<T> clazz; 
	public BaseServiceImpl() {
		// 通过反射得到T的真实类型
		ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
		this.clazz = (Class) pt.getActualTypeArguments()[0];
	}

	public void save(T entity) {
		getSession().save(entity);
	}

	public void update(T entity) {
		getSession().update(entity);
	}

	public void delete(Long id) {
		Object obj = getSession().get(clazz, id);
		getSession().delete(obj);
	}

	public T getById(Long id) {
		if (id == null) {
			return null;
		}
		return (T) getSession().get(clazz, id);
	}

	public List<T> getByIds(Long[] ids) {
		if (ids == null || ids.length == 0) {
			return Collections.EMPTY_LIST;
		}

		return getSession().createQuery(
				"FROM " + clazz.getSimpleName() + " WHERE id IN(:ids)")
				.setParameterList("ids", ids)
				.list();
	}

	public List<T> findAll() {
		return getSession().createQuery(
				"FROM " + clazz.getSimpleName())
				.list();
	}

	public PageBean getPageBean(int pageNum, HqlHelper hqlHelper) {
   		int pageSize = Configuration.getPageSize();
		List<Object> parameters = hqlHelper.getParameters();
		Map<String, Object[]> listParams = hqlHelper.getListParams();

		// 查询本页的数据列表
		Query listQuery = getSession().createQuery(hqlHelper.getQueryListHql());
		if (parameters != null && parameters.size() > 0) { // 设置参数
			for (int i = 0; i < parameters.size(); i++) {
				listQuery.setParameter(i, parameters.get(i));
			}
		}
		Set<String> keySet = listParams.keySet();
		if(keySet!=null && keySet.size()>0){ //设置集合参数
			for(String key : keySet){
				listQuery.setParameterList(key, listParams.get(key));
			}
		}
		listQuery.setFirstResult((pageNum - 1) * pageSize);
		listQuery.setMaxResults(pageSize);
		List list = listQuery.list(); // 执行查询

		// 查询总记录数
		Query countQuery = getSession().createQuery(hqlHelper.getQueryCountHql());
		if (parameters != null && parameters.size() > 0) { // 设置参数
			for (int i = 0; i < parameters.size(); i++) {
				countQuery.setParameter(i, parameters.get(i));
			}
		}
		if(keySet!=null && keySet.size()>0){ //设置集合参数
			for(String key : keySet){
				countQuery.setParameterList(key, listParams.get(key));
			}
		}
		Long count = (Long) countQuery.uniqueResult(); // 执行查询

		return new PageBean(pageNum, pageSize, list, count.intValue());
	}

	@Override
	public List getQueryList(HqlHelper hqlHelper) {
		List<Object> parameters = hqlHelper.getParameters();
		
		Map<String, Object[]> listParams = hqlHelper.getListParams();

		// 查询本页的数据列表
		Query listQuery = getSession().createQuery(hqlHelper.getQueryListHql());
		if (parameters != null && parameters.size() > 0) { // 设置参数
			for (int i = 0; i < parameters.size(); i++) {
				listQuery.setParameter(i, parameters.get(i));
			}
		}
		
		Set<String> keySet = listParams.keySet();
		if(keySet!=null && keySet.size()>0){ //设置集合参数
			for(String key : keySet){
				listQuery.setParameterList(key, listParams.get(key));
			}
		}
		return listQuery.list();
	}

	/**
	 * 获取当前可用的Session
	 * 
	 * @return
	 */
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

}
