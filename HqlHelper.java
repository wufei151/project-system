package com.soha.traffic.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.soha.traffic.base.BaseService;
import com.soha.traffic.bean.PageBean;

/**
 * 辅助sql语句的类
 * @author wufei
 *
 */
public class HqlHelper {

	private String fromClause; // From子句，必须
	private String whereClause = ""; // Where子句，可选
	private String orderByClause = ""; // OrderBy子句，可选
	private String groupByClause = ""; // OrderBy子句，可选
	private String headByClause = "";//select 选项

	private List<Object> parameters = new ArrayList<Object>(); // 参数列表
	private Map<String,Object[]> listParams = new HashMap<String, Object[]>(); //集合参数

	/**
	 * 生成From子句，默认的别名为'o'
	 * 
	 * @param clazz
	 */
	public HqlHelper(Class clazz) {
		this.fromClause = "FROM " + clazz.getSimpleName() + " o";
	}

	/**
	 * 生成From子句，使用指定的别。'
	 * 
	 * @param clazz
	 * @param alias
	 *            别名
	 */
	public HqlHelper(Class clazz, String alias) {
		this.fromClause = "FROM " + clazz.getSimpleName() + " " + alias;
	}

	/**
	 * 拼接Where子句,不带参数
	 * 
	 * @param condition
	 * @param params
	 */
	public HqlHelper addCondition(String condition) {
		// 拼接
		if (whereClause.length() == 0) {
			whereClause = " WHERE " + condition;
		} else {
			whereClause += " AND " + condition;
		}
		return this;
	}
	
	/**
	 * 拼接Where子句
	 * 
	 * @param condition
	 * @param params
	 */
	public HqlHelper addCondition(String condition, Object... params) {
		// 拼接
		if (whereClause.length() == 0) {
			whereClause = " WHERE " + condition;
		} else {
			whereClause += " AND " + condition;
		}

		// 保存参数
		if (params != null && params.length > 0) {
			for (Object obj : params) {
				parameters.add(obj);
			}
		}

		return this;
	}
	/**
	 * 如果第1个参数为true，则拼接Where子句
	 * 
	 * @param append
	 * @param condition
	 * @param params
	 */
	public HqlHelper addCondition(boolean append, String condition, Object... params) {
		if (append) {
			addCondition(condition, params);
		}
		return this;
	}
	
	/**
	 * 拼接Where子句
	 * 
	 * @param condition
	 * @param params
	 */
	public HqlHelper addListCondition(String condition, String paramKey, Object[] params) {
		// 拼接
		if (whereClause.length() == 0) {
			whereClause = " WHERE " + condition;
		} else {
			whereClause += " AND " + condition;
		}

		// 保存参数
		if (paramKey!=null && params != null) {
			listParams.put(paramKey, params);
		}

		return this;
	}
	/**
	 * 如果第1个参数为true，则拼接Where子句
	 * 
	 * @param append
	 * @param condition
	 * @param params
	 */
	public HqlHelper addListCondition(boolean append, String condition, String paramKey, Object[] params) {
		if (append) {
			addListCondition(condition, paramKey, params);
		}
		return this;
	}

	/**
	 * 拼接OrderBy子句
	 * 
	 * @param propertyName
	 *            属性名
	 * @param isAsc
	 *            true表示升序，false表示降序
	 */
	public HqlHelper addOrder(String propertyName, boolean isAsc) {
		if (orderByClause.length() == 0) {
			orderByClause = " ORDER BY " + propertyName + (isAsc ? " ASC" : " DESC");
		} else {
			orderByClause += ", " + propertyName + (isAsc ? " ASC" : " DESC");
		}
		return this;
	}

	/**
	 * 如果第1个参数为true，则拼接OrderBy子句
	 * 
	 * @param append
	 * @param propertyName
	 *            属性名
	 * @param isAsc
	 *            true表示升序，false表示降序
	 */
	public HqlHelper addOrder(boolean append, String propertyName, boolean isAsc) {
		if (append) {
			addOrder(propertyName, isAsc);
		}
		return this;
	}
	/**
	 * 拼接GroupBy子句(可选)
	 * @param propertyName
	 * @return
	 */
	public HqlHelper addGroupBy(String propertyName) {
		if (groupByClause.length() == 0) {
			groupByClause = " Group BY " + propertyName;
		} else {
			groupByClause += ", " + propertyName;
		}
		return this;
	}
	
	
	/**
	 * 拼接SELECT子句(可选)
	 * @param propertyName
	 * @return
	 */
	public HqlHelper addQueryHead(String propertyName) {
		if (headByClause.length() == 0) {
			headByClause = " SELECT " + propertyName;
		} else {
			headByClause += ", " + propertyName;
		}
		return this;
	}
	
	/**
	 * 拼接SELECT子句(可选)
	 * @param propertyName
	 * @param alias 别名
	 * @return
	 */
	public HqlHelper addQueryHead(String propertyName,String alias) {
		if (headByClause.length() == 0) {
			headByClause = " SELECT " + propertyName+" as "+ alias;
		} else {
			headByClause += ", " + propertyName+ " as "+ alias;
		}
		return this;
	}

	/**
	 * 获取生成的查询数据列表的HQL语句
	 * 
	 * @return
	 */
	public String getQueryListHql() {
		//System.out.println(headByClause +" "+ fromClause + whereClause + groupByClause + orderByClause);
		return headByClause +" "+ fromClause + whereClause + groupByClause + orderByClause;
	}

	/**
	 * 获取生成的查询总记录数的HQL语句（没有OrderBy子句）
	 * 
	 * @return
	 */
	public String getQueryCountHql() {
		return "SELECT COUNT(*) " + fromClause + whereClause;
	}

	/**
	 * 获取参数列表，与HQL过滤条件中的'?'一一对应
	 * 
	 * @return
	 */
	public List<Object> getParameters() {
		return parameters;
	}
	
    /**
     * 获取集合参数
     * @return
     */
	public Map<String, Object[]> getListParams() {
		return listParams;
	}

	/**
	 * 查询并准备分页信息（放到栈顶）
	 * 
	 * @param pageNum
	 * @param service
	 * @return
	 */
	public HqlHelper buildPageBeanForStruts2(int pageNum, BaseService<?> service) {
		PageBean pageBean = service.getPageBean(pageNum, this);
		ActionContext.getContext().getValueStack().push(pageBean);
		return this;
	}
	
	/**
	 * 查询并准备分页信息（app端）
	 * 
	 * @param pageNum
	 * @param service
	 * @return
	 */
	public PageBean buildPageBeanForAPP(int pageNum, BaseService<?> service) {
		return service.getPageBean(pageNum, this);
	}
	
	/**
	 * 查询并准备分页信息（放到栈顶）
	 * 
	 * @param pageNum
	 * @param service
	 * @return
	 */
	public List buildQueryListForStruts2(BaseService<?> service) {
		List recordList = service.getQueryList(this);
		ActionContext.getContext().put("recordList", recordList);
		return recordList;
	}
	
	public PageBean buildPageBean(int pageNum, BaseService<?> service) {
		PageBean pageBean = service.getPageBean(pageNum, this);
		ActionContext.getContext().getValueStack().push(pageBean);
		return pageBean;
	}
	/**
	 * 获得列表数据
	 * @param service
	 * @return
	 */
	public List getQueryList(BaseService<?> service){
		return service.getQueryList(this);
	}

}
