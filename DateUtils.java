package com.soha.traffic.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class DateUtils {
	public static final int YEAR_DAYS = 365; //定义一年有多少天
	/**
	 * 得到年份 2014-2050
	 * 
	 * @return
	 */
	public static List<Integer> getYearList() {
		List<Integer> yearList = new ArrayList<Integer>();
		for (int i = 2014; i <= 2050; i++) {
			yearList.add(i);
		}
		return yearList;
	}
	/**
	 * 计算两个日期间相差的天数
	 * @param dateStart
	 * @param dateEnd
	 * @return
	 */
	public static int getDiscrepantDays(Date dateStart, Date dateEnd) {
        return (int) ((dateEnd.getTime() - dateStart.getTime()) / 1000 / 60 / 60 / 24);
    }

	/**
	 * 得到月份（1-12）
	 * 
	 * @return
	 */
	public static List<Integer> getMonthList() {
		List<Integer> monthList = new ArrayList<Integer>();
		for (int i = 1; i <= 12; i++) {
			monthList.add(i);
		}
		return monthList;
	}

	/**
	 * 格式化日期的时分秒为23:59:59 例如2015-07-01 00:00:00 格式化为 2015-07-01 23:59:59
	 * 
	 * @param endTime
	 * @return 格式化后的日期
	 */
	public static Timestamp formatEndTime(Timestamp endTime) {
		Calendar instance = Calendar.getInstance();
		instance.setTime(endTime);
		instance.set(instance.get(Calendar.YEAR), instance.get(Calendar.MONTH),
				instance.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
		return new Timestamp(instance.getTime().getTime());
	}
	
	
	public static String formatTime(Timestamp dateTime,String format) {
		Calendar instance = Calendar.getInstance();
		instance.setTime(dateTime);
		
		SimpleDateFormat sf = new SimpleDateFormat(format);
		return sf.format(instance.getTime());
	}
	
	public static Timestamp parseTime(String time,String format){
		SimpleDateFormat sf = new SimpleDateFormat(format);
		try {
			Date parse = sf.parse(time);
			return new Timestamp(parse.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取当前时间对象
	 * 
	 * @param date
	 * @return
	 */
	public static Date getNowDate() {
		return Calendar.getInstance().getTime();
	}
	/**
	 * 获取当前时间对象
	 * 
	 * @param date
	 * @return
	 */
	public static Timestamp getNowTimestamp(){
		return new Timestamp(new Date().getTime());
	}

	/**
	 * 获取当前时间,并将其转换为形式:"2006-06-06 09:54:29"的字符串
	 * 
	 * @param date
	 * @return
	 */
	public static Date getNowDateAndTime() {
		return DateUtils.convertToDateAndTime(DateUtils.getNowDateAndTimeStr());
	}

	/**
	 * 获取当前时间,并将其转换为形式:"2006-06-06"的字符串
	 * 
	 * @param date
	 * @return
	 */
	public static String getNowDateStr() {
		return DateUtils.convertToDateStr(getNowDate());
	}

	/**
	 * 获取当前时间,并将其转换为形式:"2006-06-06 09:54:29"的字符串
	 * 
	 * @param date
	 * @return
	 */
	public static String getNowDateAndTimeStr() {
		return DateUtils.convertToDateAndTimeStr(getNowDate());
	}

	/**
	 * 更具给定的pattern时间形式进行时间字符串转换得到当前时间
	 * 
	 * @param pattern
	 * @return
	 */
	public static String getNowDateStrByPattern(String pattern) {
		return getDateStrByPattern(getNowDate(), pattern);
	}

	/**
	 * 更具给定的pattern时间形式进行时间字符串转换得到当前时间
	 * 
	 * @param pattern
	 * @return
	 */
	public static String getNowDateStrByPattern(Locale locale, String pattern) {
		return getDateStrByPattern(getNowDate(), locale, pattern);
	}

	/**
	 * 更具给定的pattern时间形式进行时间date的字符串转换
	 * 
	 * @param pattern
	 * @param date
	 * @return
	 */
	public static String getDateStrByPattern(Date date, String pattern) {
		if (date == null)
			return null;
		if (pattern == null)
			return convertToDateAndTimeStr(date);
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(date);
	}

	/**
	 * 更具给定的pattern时间形式进行时间date的字符串转换
	 * 
	 * @param pattern
	 * @param date
	 * @return
	 */
	public static String getDateStrByPattern(Date date, Locale locale,
			String pattern) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, locale);
		return dateFormat.format(date);
	}

	/**
	 * 把String类型的日期表示转换为Date类型,转换的类型必须2008-09-27"形式,忽略时分秒
	 * 
	 * @param dateStr
	 * @return
	 */
	public static String convertToDateStr(Date date) {
		if (date == null)
			return null;
		DateFormat formatParse = new SimpleDateFormat("yyyy-MM-dd");
		return formatParse.format(date);
	}

	/**
	 * 把String类型的日期表示转换为Date类型,转换的类型必须2008-09-27 11:00:00"形式,忽略时分秒
	 * 
	 * @param dateStr
	 * @return
	 */
	public static String convertToDateAndTimeStr(Date date) {
		if (date == null)
			return null;
		DateFormat formatParse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return formatParse.format(date);
	}

	/**
	 * 把String类型的日期表示转换为Date类型,转换的类型必须2008-09-27"形式,忽略时分秒
	 * 
	 * @param dateStr
	 * @return
	 */
	public static Date convertToDate(String dateStr) {
		if (dateStr == null || "".equals(dateStr))
			return null;
		DateFormat formatParse = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return formatParse.parse(dateStr);
		} catch (ParseException e) {
			throw new RuntimeException(
					"传入的String类型格式错误,正确的格式必须为‘yyyy-MM-dd�?�?006-02-07");
		}
	}

	/**
	 * 将日期类型的时分秒置为23:59:59的日期
	 * 
	 * @param date
	 * @return
	 */
	public static Date convertToEndDate(Date date) {
		return convertToDateAndTime(convertToDateStr(date) + " 23:59:59");
	}
	
	/**将string类型的带有十分秒的时间去掉是分秒部分**/
	public static String converToString(String strDate){
		String[] strArray = strDate.split(" ");
		String formatStringDate = strArray[0];
		return formatStringDate;
		
	}
	
	/**
	 * 将日期类型的时分秒置为00:00:00的日期
	 * 
	 * @param date
	 * @return
	 */
	public static Date convertToDate(Date date) {
		return convertToDateAndTime(convertToDateStr(date) + " 00:00:00");
	}

	/**
	 * 把String类型的日期表示转换为Date类型,转换的类型必须2006-02-07 17:09:30"形式
	 * 
	 * @param dateStr
	 * @return
	 */
	public static Date convertToDateAndTime(String dateStr) {
		if (dateStr == null || "".equals(dateStr))
			return null;
		DateFormat formatParse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return formatParse.parse(dateStr);
		} catch (ParseException e) {
			throw new RuntimeException(
					"传入的String类型格式错误,正确的格式必须为‘yyyy-MM-dd HH:mm:ss 2006-02-07 17:09:30");
		}
	}

	/**
	 * 把String类型的日期表示转换为Date类型,转换的类型必须2006-02-07 17:09:30"形式,忽略时分秒
	 * 
	 * @param dateStr
	 * @return
	 */
	public static Date convertToDateAndTime(String dateStr, String parsePattern) {
		if (dateStr == null)
			return null;
		DateFormat formatParse = new SimpleDateFormat(parsePattern);
		try {
			return formatParse.parse(dateStr);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(
					"传入的String类型格式错误,正确的格式必须为‘yyyy-MM-dd HH:mm:ss006-02-07 17:09:30");
		}
	}

	/**
	 * 通过年份，得到年初的第一天的第一时刻，如2010-01-01 00:00:00.0
	 * 
	 * @param year
	 * @return
	 */
	public static Date getFristDate(int year) {
		return getFristDate(year,
				Calendar.getInstance().getActualMinimum(Calendar.MONTH));
	}

	/**
	 * 获取到上一周的周一输出字符串2015-11-10 00:00:00
	 * 
	 * @return
	 */
	public static String getFristBeforeWeek(){
		Calendar cal = Calendar.getInstance();
		//n为推迟的周数，1本周，-1向前推迟一周，2下周，依次类推
		int n = -1;
		String monday;
		cal.add(Calendar.DATE, n*7);
		cal.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
		monday = new SimpleDateFormat("yyyy-MM-dd 00:00:00").format(cal.getTime());
		
		return monday;
	}
	
	/**
	 * 获取到上一周的周日输出字符串2015-11-10 23:59:59
	 * 上一周最后一天
	 * 
	 * @return
	 */
	public static String getLastBeforeWeek(){
		Calendar cal = Calendar.getInstance();
		//n为推迟的周数，1本周，-1向前推迟一周，2下周，依次类推
		int n = -1;
		String sunday;
		//cal.add(Calendar.DATE, n*7);
		cal.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
		sunday = new SimpleDateFormat("yyyy-MM-dd 23:59:59").format(cal.getTime());
		return sunday;
	}

	/**
	 * 通过年份，得到年初的����的最后时刻，如：2010-12-30 23:59:59.999
	 * 
	 * @param year
	 * @return
	 */
	public static Date getLastDate(int year) {
		return getLastDate(year,
				Calendar.getInstance().getActualMaximum(Calendar.MONTH));
	}

	/**
	 * 通过年份、月份，得到年初的第��的第��刻，如：2010-01-01 00:00:00.0
	 * 
	 * @param year
	 *            如： 2008, 2009, 2010
	 * @param month
	 *            如： Calendar.JANUARY, Calendar.FEBRUARY
	 * @return
	 */
	public static Date getFristDate(int year, int month) {
		return getFristDate(year, month, Calendar.getInstance()
				.getActualMinimum(Calendar.DAY_OF_MONTH));
	}

	/**
	 * 通过年份、月份，得到年初的最后一天的��时刻，如010-12-30 23:59:59.999
	 * 
	 * @param year
	 *            如： 2008, 2009, 2010
	 * @param month
	 *            如： Calendar.JANUARY, Calendar.FEBRUARY
	 * @return
	 */
	public static Date getLastDate(int year, int month) {
		return getLastDate(year, month, Calendar.getInstance()
				.getActualMaximum(Calendar.DAY_OF_MONTH));
	}

	/**
	 * 通过年份、月份月份中的某天，得到年初的第一天的第一时刻，如010-01-01 00:00:00.0
	 * 
	 * @param year
	 *            如： 2008, 2009, 2010
	 * @param month
	 *            如： Calendar.JANUARY, Calendar.FEBRUARY
	 * @return
	 */
	public static Date getFristDate(int year, int month, int dayOfMonth) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month);
		c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		c.set(Calendar.HOUR_OF_DAY, c.getActualMinimum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getActualMinimum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMinimum(Calendar.SECOND));
		c.set(Calendar.MILLISECOND, c.getActualMinimum(Calendar.MILLISECOND));
		return c.getTime();
	}

	/**
	 * 通过年份、月份月份中的某天，得到年初的的最后时刻，如：2010-12-30 23:59:59.999
	 * 
	 * @param year
	 *            如： 2008, 2009, 2010
	 * @param month
	 *            如： Calendar.JANUARY, Calendar.FEBRUARY
	 * @return
	 */
	public static Date getLastDate(int year, int month, int dayOfMonth) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month);
		c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		c.set(Calendar.HOUR_OF_DAY, c.getActualMaximum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));
		c.set(Calendar.MILLISECOND, c.getActualMaximum(Calendar.MILLISECOND));
		return c.getTime();
	}

	public static int getYear(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.YEAR);
	}

	public static int getMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.MONTH);
	}

	public static int[] getMonths() {
		return new int[] { Calendar.JANUARY, Calendar.FEBRUARY, Calendar.MARCH,
				Calendar.APRIL, Calendar.MAY, Calendar.JUNE, Calendar.JULY,
				Calendar.AUGUST, Calendar.SEPTEMBER, Calendar.OCTOBER,
				Calendar.NOVEMBER, Calendar.DECEMBER };
	}

	public static String convertMsToDDHHSSMM(long ms) {
		long days = ms / (1000 * 60 * 60 * 24);
		long hours = (ms % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
		long minutes = (ms % (1000 * 60 * 60)) / (1000 * 60);
		long seconds = (ms % (1000 * 60)) / 1000;
		return days + "日" + hours + "小时" + minutes + "分" + seconds + "秒";
	}

	public static String convertMsToHHSSMM(long ms) {
		long hours = ms / (1000 * 60 * 60);
		long minutes = (ms % (1000 * 60 * 60)) / (1000 * 60);
		long seconds = (ms % (1000 * 60)) / 1000;
		return hours + "小时" + minutes + "分" + seconds + "秒";
	}

	// public static void main(String[] args) {
	// System.out.println(DateUtil.convertToDateAndTimeStr(getLastDayOfYearMonthDate()));
	// }

	public static String dateDiff(String startTime, String endTime) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date stime = null;
		java.util.Date etime = null;
		try {
			stime = df.parse(startTime);
			etime = df.parse(endTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long l = etime.getTime() - stime.getTime();
		long day = l / (24 * 60 * 60 * 1000);
		long hour = (l / (60 * 60 * 1000) - day * 24);
		long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
		long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
		return "" + hour + "小时" + min + "分" + s + "秒";
	}

	public static Date getDateTime(String createTime) {
		if (createTime == null || "".equals(createTime)) {
			return new Date();
		} else {
			try {
				return DateUtils.convertToDateAndTime(createTime);
			} catch (Exception ex) {
				return new Date();
			}
		}
	}

	public static String getMonth(int month) {
		if (Calendar.JANUARY == month) {
			return "一月";
		} else if (Calendar.FEBRUARY == month) {
			return "二月";
		} else if (Calendar.MARCH == month) {
			return "三月";
		} else if (Calendar.APRIL == month) {
			return "四月";
		} else if (Calendar.MAY == month) {
			return "五月";
		} else if (Calendar.JUNE == month) {
			return "六月";
		} else if (Calendar.JULY == month) {
			return "七月";
		} else if (Calendar.AUGUST == month) {
			return "八月";
		} else if (Calendar.SEPTEMBER == month) {
			return "九月";
		} else if (Calendar.OCTOBER == month) {
			return "十月";
		} else if (Calendar.NOVEMBER == month) {
			return "十一月";
		} else if (Calendar.DECEMBER == month) {
			return "十二月";
		}
		return String.valueOf(month);
	}

	/**
	 * 获取8位不重复随机码（取当前时间戳转化为十六进制）
	 * 
	 * @author ShelWee
	 * @param time
	 * @return
	 */
	public static String toHex(long time) {
		return Integer.toHexString((int) time);
	}

	public static String toDate(Date date) {
		// 获得时间戳文件夹名
		SimpleDateFormat format = new SimpleDateFormat("MMddssssss");
		String dates = format.format(date);
		return dates;
	}
	
	/**
	 * 随机生成五位随机数
	 * @return
	 */
	public static Integer getRandom(){
		Random random = new Random();
		int rd = 10000+random.nextInt(90000);
		return rd;
	}
	/**
	 * 
	 * @param dateString yyyy/mm/dd
	 * @return
	 */
	public static String parseDateString(String dateString){
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd");
			Date parse = sf.parse(dateString);
			
			SimpleDateFormat sf1 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
			return sf1.format(parse);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 获得月份的第一天日期，月份索引从1开始，比如1月是1
	 * @param month
	 * @return
	 */
	public static Calendar getStartDateByMonth(int month){
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR),month-1,1,0,0,0);
		return calendar;
	}
	
	/**
	 * 获得月份的最后一天的日期，月份索引从1开始，比如1月是1
	 * @param month
	 * @return
	 */
	public static Calendar getEndDateByMonth(int month){
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR),month-1,1,0,0,0);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return calendar;
	}
	/**
	 * 格式化日期，得到此日期月份第第一天，转化错误时，返回当前月份的第一天
	 * @param dateFormat
	 * @return
	 */
	public static Date getMonthFirstDay(String date,String format){
		try {
			SimpleDateFormat sf = new SimpleDateFormat(format);
			Date parse = sf.parse(date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(parse);
			calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),1,0,0,0);
			return calendar.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//否则，返回当前月份的第一天
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),1,0,0,0);
		return calendar.getTime();
	}
	/**
	 * 格式化日期，得到此日期月份第最后一天，转化错误时，返回当前月份的最后一天
	 * @param dateFormat
	 * @return
	 */
	public static Date getMonthEndDay(String date,String format){
		try {
			SimpleDateFormat sf = new SimpleDateFormat(format);
			Date parse = sf.parse(date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(parse);
			calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),
					calendar.getActualMaximum(Calendar.DAY_OF_MONTH),0,0,0);
			return calendar.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//否则，返回当前月份的最后一天
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),
				calendar.getActualMaximum(Calendar.DAY_OF_MONTH),0,0,0);
		return calendar.getTime();
	}
	/**
	 * 格式化日期，得到此日期月份第第一天，转化错误时，返回当前月份的第一天
	 * @param dateFormat
	 * @return
	 */
	public static Calendar getCalendarByFormatDate(String date,String format){
		try {
			SimpleDateFormat sf = new SimpleDateFormat(format);
			Date parse = sf.parse(date);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(parse);
			calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),1,0,0,0);
			return calendar;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		//否则，返回当前月份的第一天
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),1,0,0,0);
		return calendar;
	}
	
	public static Calendar getFirstDay(int dateType){
		Calendar calendar = Calendar.getInstance();
		switch (dateType) {
		case CommonProperty.DAY:
			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE),
					0, 0, 0);
			break;
		case CommonProperty.MONTH:
			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1,
					0, 0, 0);
			break;
		case CommonProperty.YEAR:
			calendar.set(calendar.get(Calendar.YEAR), 0, 1, 0, 0, 0);
			break;
		}
		return calendar;
	}
	
	public static Calendar getEndDay(int dateType){
		Calendar calendar = Calendar.getInstance();
		switch (dateType) {
		case CommonProperty.DAY:
			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE),
					23, 59, 59);
			break;
		case CommonProperty.MONTH:
			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.getActualMaximum(Calendar.DAY_OF_MONTH),
					23, 59, 59);
			break;
		case CommonProperty.YEAR:
			calendar.set(calendar.get(Calendar.YEAR), 11, 31, 23, 59, 59);
			break;
		}
		return calendar;
	}
	
	public static Calendar getFirstDay(int dateType,int year,int month,int date){
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, date);
		switch (dateType) {
		case CommonProperty.DAY:
			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE),
					0, 0, 0);
			break;
		case CommonProperty.MONTH:
			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1,
					0, 0, 0);
			break;
		case CommonProperty.YEAR:
			calendar.set(calendar.get(Calendar.YEAR), 0, 1, 0, 0, 0);
			break;
		}
		return calendar;
	}
	
	public static Calendar getEndDay(int dateType,int year,int month,int date){
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, date);
		switch (dateType) {
		case CommonProperty.DAY:
			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE),
					23, 59, 59);
			break;
		case CommonProperty.MONTH:
			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.getActualMaximum(Calendar.DAY_OF_MONTH),
					23, 59, 59);
			break;
		case CommonProperty.YEAR:
			calendar.set(calendar.get(Calendar.YEAR), 11, 31, 23, 59, 59);
			break;
		}
		return calendar;
	}
}
