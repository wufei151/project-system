package com.soha.traffic.base;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.soha.traffic.bean.SystemUser;
import com.soha.traffic.interceptor.LoginInterceptor;
import com.soha.traffic.service.CalcProjectDetailService;
import com.soha.traffic.service.CalcProjectForeginDetailService;
import com.soha.traffic.service.CalcProjectForeginDisDetailService;
import com.soha.traffic.service.CalcProjectHeaderService;
import com.soha.traffic.service.ContractForeignPayDetailService;
import com.soha.traffic.service.ContractForeignService;
import com.soha.traffic.service.ContractHiredIncomeDetailService;
import com.soha.traffic.service.ContractHiredService;
import com.soha.traffic.service.CostItemDetailService;
import com.soha.traffic.service.CostItemHeaderService;
import com.soha.traffic.service.CostItemReviewDetailService;
import com.soha.traffic.service.CostPersonDetailService;
import com.soha.traffic.service.CostPresonHeaderService;
import com.soha.traffic.service.CostTravelItemDetailService;
import com.soha.traffic.service.CostTravelReviewItemDetailService;
import com.soha.traffic.service.CustomerManagerService;
import com.soha.traffic.service.DepartmentService;
import com.soha.traffic.service.ExamineMonthPeopleService;
import com.soha.traffic.service.ExamineYearPeopleService;
import com.soha.traffic.service.FixedWorkItemService;
import com.soha.traffic.service.FixedWorkTemplateService;
import com.soha.traffic.service.PorjectOrgWrokItemService;
import com.soha.traffic.service.ProjectDaysDetailService;
import com.soha.traffic.service.ProjectDaysHeaderService;
import com.soha.traffic.service.ProjectDaysWorkflowDetailService;
import com.soha.traffic.service.ProjectDaysWorkflowService;
import com.soha.traffic.service.ProjectExamineMonthPeopleService;
import com.soha.traffic.service.ProjectExamineYearPeopleService;
import com.soha.traffic.service.ProjectInfoService;
import com.soha.traffic.service.ProjectOrgForeignDaysDetailService;
import com.soha.traffic.service.ProjectOrgInnerDaysDetailService;
import com.soha.traffic.service.ProjectOrgService;
import com.soha.traffic.service.ProjectProgressService;
import com.soha.traffic.service.ProjectTimeService;
import com.soha.traffic.service.SystemDicCategoryService;
import com.soha.traffic.service.SystemDicItemService;
import com.soha.traffic.service.SystemPrivilegeService;
import com.soha.traffic.service.SystemRoleService;
import com.soha.traffic.service.SystemUserService;

public abstract class BaseAction extends ActionSupport implements
ServletResponseAware, ServletRequestAware{
	
	protected HttpServletResponse response;

	protected HttpServletRequest request;
	
	protected String jsonResult;
	
	@Resource
	protected SystemUserService systemUserService;
	@Resource
	protected SystemRoleService systemRoleService;
	@Resource
	protected SystemPrivilegeService privilegeService;
	@Resource
	protected DepartmentService departmentService;
	@Resource
	protected ProjectInfoService projectInfoService;
	@Resource
	protected SystemDicCategoryService systemDicCategroyService;
	@Resource
	protected SystemDicItemService systemDicItemService;
	@Resource
	protected CustomerManagerService customerManagerService;
	@Resource
	protected FixedWorkItemService fixedWorkItemService;
	@Resource
	protected ContractHiredService contractHiredService;
	@Resource
	protected ContractForeignService contractForeignService;
	@Resource
	protected ProjectOrgService projectOrgService;
	@Resource
	protected ProjectTimeService projectTimeService;
	@Resource
	protected CostItemHeaderService costItemHeaderService;
	@Resource
	protected CostItemDetailService costItemDetailService;
	@Resource
	protected PorjectOrgWrokItemService porjectOrgWrokItemService;
	@Resource
	protected ContractHiredIncomeDetailService contractHiredIncomeDetailService;
	@Resource
	protected ContractForeignPayDetailService contractForeignPayDetailService;
	@Resource
	protected ProjectOrgForeignDaysDetailService projectOrgForeignDaysDetailService;
	@Resource
	protected ProjectOrgInnerDaysDetailService projectOrgInnerDaysDetailService;
	@Resource
	protected CostTravelItemDetailService costTravelItemDetailService;
	@Resource
	protected FixedWorkTemplateService fixedWorkTemplateService;
	@Resource
	protected CalcProjectHeaderService calcProjectHeaderService;
	@Resource
	protected CalcProjectDetailService calcProjectDetailService;
	@Resource
	protected CalcProjectForeginDetailService calcProjectForeginDetailService;
	@Resource
	protected ProjectProgressService projectProgressService;
	@Resource
	protected ProjectDaysHeaderService projectDaysHeaderService;
	@Resource
	protected ProjectDaysDetailService projectDaysDetailService;
	@Resource
	protected ProjectDaysWorkflowService projectDaysWorkflowService;
	@Resource
	protected ProjectDaysWorkflowDetailService projectDaysWorkflowDetailService;
	@Resource
	protected CostPresonHeaderService costPresonHeaderService;
	@Resource
	protected CostPersonDetailService costPersonDetailService;
	@Resource
	protected ExamineYearPeopleService examineYearPeopleService;
	@Resource
	protected ProjectExamineYearPeopleService projectExamineYearPeopleService;
	@Resource
	protected CalcProjectForeginDisDetailService calcProjectForeginDisDetailService;
	@Resource
	protected CostItemReviewDetailService costItemReviewDetailService;
	@Resource
	protected CostTravelReviewItemDetailService costTravelReviewItemDetailService;
	@Resource
	protected ExamineMonthPeopleService examineMonthPeopleService;
	@Resource
	protected ProjectExamineMonthPeopleService projectExamineMonthPeopleService;
	
	
	
	
	
	// 页码默认为第1页
	protected int pageNum = 1;
	
	
	/**
	 * 获取当前登录的用户
	 * 
	 * @return
	 */
	protected SystemUser getCurrentUser() {
		return (SystemUser) ActionContext.getContext().getSession().get(LoginInterceptor.USER_SESSION_KEY);
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;

	}

	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;

	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public String getJsonResult() {
		return jsonResult;
	}

	public void setJsonResult(String jsonResult) {
		this.jsonResult = jsonResult;
	}
	/**
	 * 输出响应信息
	 * @param str
	 */
	protected void responsePrint(String str){
		try {
			response.setContentType("text/html;charset=utf-8");
			response.getWriter().print(str.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
}
