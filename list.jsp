<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="t" uri="/traffic-ext"%>

<s:form action="projectInfoAction_list">
    <div class="all" id="pcont">
    <div class="position">
    <span><img src="${pageContext.request.contextPath}/style/default/images/pico.png" /></span>
   <font>当前位置：</font><s:a action="homeAction_index">首页</s:a>><a href="javascript:void(0)" onclick="toTarget('${pageContext.request.contextPath}/projectInfoAction_list.action')">项目管理</a>>
   <a href="javascript:void(0)" onclick="toTarget('${pageContext.request.contextPath}/projectInfoAction_list.action')">项目信息管理</a>
    </div>
		<div class="cl-mcont">
			<div class="serchbox">
				<div>
					<div class="panel panel-info filter-preview">
						<div class="panel-heading" style="font-size: 16px;">
							<span
								class="yuimenubaritemlevel0 yuimenubaritemlabel-hassubmenu yuimenubaritemlabel"><i
								class="fa fa-tasks"></i> 项目查询</span>
						</div>
						<div style="height: 120px; padding: 5px;">
							<div class="col-sm-8 col-md-8">
								<div class="form-horizontal" style="text-align: center;">
									<div class="form-group">
										<label for="projectName" class="col-sm-2 control-label">项目名称：</label>
										<div class="col-sm-4">
											<s:textfield id="conditionProjectName"
												name="conditionProjectName" cssClass="form-control"></s:textfield>
										</div>
										<label for="projectNum" class="col-sm-2 control-label">项目编号：</label>
										<div class="col-sm-4">
											<s:textfield id="conditionProjectNum"
												name="conditionProjectNum" cssClass="form-control"></s:textfield>
										</div>
									</div>
									<div class="form-group">
										<label for="conditionMonth" class="col-sm-2 control-label">按月份：</label>
										<div class="col-sm-4">
											<s:textfield id="conditionMonth" name="conditionMonth"
												cssClass="form-control" placeholder="YYYY/MM"></s:textfield>
										</div>
										<label for="conditionPType" class="col-sm-2 control-label">按项目类型：</label>
										<div class="col-sm-4">
											<s:select id="conditionPType" name="conditionPType"
												list="#pTypeList" listKey="itemCode" listValue="itemValue"
												cssClass="form-control" headerKey="" headerValue="请选择"></s:select>
										</div>
									</div>
								    <div class="form-group">
											<label for="conditionWorkStage"
												class="col-sm-2 control-label">按项目阶段：</label>
											<div class="col-sm-4">
												<s:select id="conditionWorkStage" name="conditionWorkStage"
													list="#workStageList" listKey="itemCode"
													listValue="itemValue" cssClass="form-control" headerKey=""
													headerValue="请选择"></s:select>
											</div>
											<div>
											<a href="javascript:void(0)" onclick="submitForm();"
											class="btn btn-primary">查询</a>
											</div>
									  </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tablebox">
					<div class="col-md-12">
						<div class="block-flat">
							<div class="header">
								<t:a href="javascript:void(0)"
									onclick="toTarget('${pageContext.request.contextPath}/projectInfoAction_addUI.action')"
									cssClass="btn btn-primary" label="新增项目" />
							</div>
							<div class="content">
								<div class="table-responsive">
									<table class="table table-bordered" id="datatable-icons">
										<thead>
											<tr>
												<th width="20%">项目编号</th>
												<th width="25%">项目名称</th>
												<th width="10%">项目时间</th>
												<th width="10%">项目类型</th>
												<th width="10%">项目阶段</th>
												<th width="25%" class="center">操作</th>
											</tr>
										</thead>
										<tbody>
											<s:iterator value="recordList">
												<tr>
													<td>${PCode}</td>
													<td>${fullName}</td>
													<td><s:date name="PTime" format="yyyy年" />
													</td>
													<td><s:iterator value="#pTypeList">
															<s:if test="itemCode==PType">
                                            ${itemValue}
                                          </s:if>
														</s:iterator></td>
													<td><s:iterator value="#workStageList">
															<s:if test="itemCode==workStage">
                                            ${itemValue}
                                          </s:if>
														</s:iterator></td>
													<td class="center"><t:a href="javascript:void(0)"
															onclick="toTarget('${pageContext.request.contextPath}/projectInfoAction_editUI.action?id=${id}')"
															label="修改" /> <t:a href="javascript:void(0)"
															onclick="return delConfirmToTarget('${pageContext.request.contextPath}/projectInfoAction_delete.action?id=${id}');"
															label="删除" /> <t:a href="javascript:void(0)"
															onclick="toTarget('${pageContext.request.contextPath}/projectOrgAction_list.action?projectId=${id}')"
															label="组织管理" /> <t:a href="javascript:void(0)"
															onclick="toTarget('${pageContext.request.contextPath}/calcProjectHeaderAction_list.action?projectId=${id}')"
															label="框算管理" /> <t:a href="javascript:void(0)"
															onclick="toTarget('${pageContext.request.contextPath}/projectProgressAction_list.action?projectId=${id}')"
															label="进度管理" /> <t:a href="javascript:void(0)"
															onclick="toTarget('${pageContext.request.contextPath}/projectDaysHeaderAction_list.action?projectId=${id}')"
															label="分配工日" /></td>
												</tr>
											</s:iterator>
										</tbody>
									</table>


									<!-- 分页信息 -->

									<%@ include file="/WEB-INF/jsp/public/pageView.jspf"%>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
     </s:form>
     
     <script type="text/javascript">
         $("#conditionMonth").datetimepicker({format: 'yyyy/mm',language: 'zh-CN',startView:3,minView: 3,autoclose:"true"});
     </script>